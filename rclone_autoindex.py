#!/usr/bin/env python3

import argparse
import json
from sys import stdin, stdout, stderr
from collections import defaultdict
from pathlib import Path
from io import StringIO
try:
    import htmlmin
    MINIMIZE = True
except ImportError:
    print("htmlmin not found, won't minify generated HTML. `pip install htmlmin` to enable this", file=stderr)
    MINIMIZE = False

INDEX_FILE = "index.html"

def mint_index(dir, paths, basedir, root=None):
    content = StringIO()
    if not str(dir) in {"/", ".", ""}:  # is not a base directory
        print(DIR.format(link="..", link_text="&#x21B0;"), file=content)
    for path in sorted(paths, key=lambda p: (0 if p["IsDir"] else 1, p["Name"])):
        name = path["Name"]
        if name.lower() == INDEX_FILE.lower():
            continue
        if path["IsDir"]:
            print(DIR.format(link=name, link_text=f"&#128193; {name}"), file=content)
        else:
            link = name
            if root is not None:
                link = root.rstrip('/') + '/' + path["Path"].lstrip('/')
            print(FILE.format(link=link, link_text=name, size=path["Size"]), file=content)

    gallery = Path(basedir, dir, "gallery.html")
    if gallery.is_file():
        print(FILE.format(link="gallery.html", link_text="Image Gallery", size=gallery.stat().st_size), file=content)
    out = Path(basedir, dir, INDEX_FILE)
    outdir = out.parent
    if not outdir.is_dir():
        outdir.mkdir(parents=True)
    html = HTML.format(css=CSS, current_directory=dir.name, content=content.getvalue())
    if MINIMIZE:
        html = htmlmin.minify(html, remove_comments=True, remove_empty_space=True)
    with out.open("w") as fh:
        fh.write(html)


def gather_dirs(paths):
    res = defaultdict(list)
    for entry in paths:
        p = Path(entry["Path"])
        res[p.parent].append(entry)
    return res 


def main():
    ap = argparse.ArgumentParser("rclone_autoindex")
    ap.add_argument("--cdn-root", "-r", type=str,
            help="CDN root directory")
    ap.add_argument("--output", "-o", type=str, required=True,
            help="Output directory")
    ap.add_argument("input", nargs="?", default=stdin, type=argparse.FileType('r'))
    args = ap.parse_args()

    paths = json.load(args.input)

    bydir = gather_dirs(paths)
    n = 0
    for dir, paths in bydir.items():
        mint_index(dir, paths, basedir=args.output, root=args.cdn_root)
        print(f"Done {dir}, had {len(paths)} entries", file=stderr)
        n += 1
    print(f"Added {n} index entries.")


CSS = """
<style>
body {
    background: #f4f4f4;
    margin: 2em 1.5em;
}
li {
    font-family: sans-serif;
    font-size: 12pt;
    line-height: 14pt;
    list-style:none;
    list-style-type:none;
    padding: 3px 10px;
    margin: 3px 15px;
    display: block;
    clear:both;
}
.content {
    width: 600px;
    background-color: white;
    margin-bottom: 5em;
    padding-bottom: 3em;
    -webkit-box-shadow: rgba(89, 89, 89, 0.449219) 2px 1px 9px 0px;
    -moz-box-shadow: rgba(89, 89, 89, 0.449219) 2px 1px 9px 0px;
    box-shadow: rgba(89, 89, 89, 0.449219) 2px 1px 9px 0px;
    border: 0;
    border-radius: 11px;
    -moz-border-radius: 11px;
    -webkit-border-radius: 11px;
    height: 96%;
    min-height: 90%;
}
.size {
    float: right;
    color: gray;
}
h1 {
    padding: 10px;
    margin: 15px;
    font-size:13pt;
    border-bottom: 1px solid lightgray;
}
a {
    font-weight: 500;
    perspective: 600px;
    perspective-origin: 50% 100%;
    transition: color 0.3s;
    text-decoration: none;
    color: #060606;
}
a:hover,
a:focus {
    color: #e74c3c;
}
a::before {
    background-color: #fff;
    transition: transform 0.2s;
    transition-timing-function: cubic-bezier(0.7,0,0.3,1);
    transform: rotateX(90deg);
    transform-origin: 50% 100%;
}
a:hover::before,
a:focus::before {
    transform: rotateX(0deg);
}
a::after {
    border-bottom: 2px solid #fff;
}
</style>
"""

HTML = """
<!DOCTYPE html>
<html>
<head>
{css}
</head>
<body>
<div class="content">
<h1>{current_directory}</h1>
{content}
</div>
</body>
</html>
"""

DIR = """
<li><a style="display:block; width:100%" href="{link}/index.html">{link_text}</a></li>
"""

FILE = """
<li>&#x1f4c4; <a href="{link}">{link_text}</a><span class="size">{size}</span></li>
"""


if __name__ == "__main__":
    main()
