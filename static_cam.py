#!/usr/bin/env python3
import argparse
import json
from sys import stdin, stdout, stderr
import re
from collections import defaultdict
from pathlib import Path
from io import StringIO, BytesIO
import traceback
from concurrent.futures import ThreadPoolExecutor, as_completed

import requests
from jinja2 import Template
from PIL import Image
try:
    import htmlmin
    MINIMIZE = True
except ImportError:
    print("htmlmin not found, won't minify generated HTML. `pip install htmlmin` to enable this", file=stderr)
    MINIMIZE = False

INDEX_FILE = "index.html"
GALLERY_FILE = "gallery.html"

def get_image_wh(url):
    bio = BytesIO()
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        for chunk in r.iter_content():
            bio.write(chunk)
    img = Image.open(bio)
    return img.size

def make_psjson(files, cdn_root, width=None, height=None):
    psjs = []
    for file in files:
        if file["IsDir"]:
            continue
        if not file["Name"].endswith("jpg"):
            continue
        url = cdn_root.rstrip("/") + "/" + file["Path"].lstrip("/")
        w, h = width, height
        if width is None and height is None:
            try:
                width, height = get_image_wh(url)
                w, h = width, height
            except:
                print(f"Failed to get image with/height for '{url}'", file=stderr)
                if stderr.isatty():
                    traceback.print_exc(file=stderr)
                w, h = 600, 400
                return psjs
        psjs.append({
            "src": url,
            "w": w,
            "h": h,
            "title": file["Name"], #todo a nice title
            })
    return psjs


def mint_camera(dir, paths, basedir, root, width=None, height=None):
    psjs = json.dumps(make_psjson(paths, root, width=width, height=height))
    out = Path(basedir, dir, GALLERY_FILE)
    outdir = out.parent
    if not outdir.is_dir():
        outdir.mkdir(parents=True)
    html = HTML.render(current_directory=dir.name, itemjson=psjs)
    if MINIMIZE:
        html = htmlmin.minify(html, remove_comments=True, remove_empty_space=True)
    with out.open("w") as fh:
        fh.write(html)


def gather_dirs(paths, level = 1):
    res = defaultdict(list)
    for entry in paths:
        p = Path(entry["Path"])
        dir = p.parent
        while True:
            if len(dir.parts) < level:
                break
            res[dir].append(entry)
            dir = dir.parent
    return res 


def main():
    ap = argparse.ArgumentParser("rclone_autoindex")
    ap.add_argument("--grep", "-g", type=str, default=None,
            help="Only include files with re.search match for this string.")
    ap.add_argument("--assume-wh", "-s", type=int, default=(None, None), nargs=2,
            help="Assume width and height to be --assume-wh w h")
    ap.add_argument("--cdn-root", "-r", type=str, required=True,
            help="CDN root directory")
    ap.add_argument("--timestream-level", "-l", type=int, default=3,
            help="Directory level of 'timestream' folders. e.g. org/camera/camera_orig/ = 3. see pathlib.Path.parts")
    ap.add_argument("--output", "-o", type=str, required=True,
            help="Output directory")
    ap.add_argument("input", nargs="?", default=stdin, type=argparse.FileType('r'))
    args = ap.parse_args()

    paths = json.load(args.input)

    bydir = gather_dirs(paths, level=args.timestream_level)
    n = 0
    w, h = args.assume_wh
    print(w, h)
    for dir, paths in bydir.items():
        if args.grep:
            if re.search(args.grep, str(dir)) is None:
                print(f"Skipping '{dir}'", file=stderr)
                continue
        mint_camera(dir, paths, basedir=args.output, root=args.cdn_root, width=w, height=h)
        print(f"Done {dir}, had {len(paths)} entries", file=stderr)
        n += 1
    print(f"Added {n} index entries.")



# need to fill in: current_directory = title of gallery; itemjson = json string of gallery contents.
HTML = Template("""<!DOCTYPE html>
<html>
<head>
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe.min.css" integrity="sha256-sCl5PUOGMLfFYctzDW3MtRib0ctyUvI9Qsmq2wXOeBY=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/default-skin/default-skin.min.css" integrity="sha256-BFeI1V+Vh1Rk37wswuOYn5lsTcaU96hGaI7OUVCLjPc=" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe.min.js" integrity="sha256-UplRCs9v4KXVJvVY+p+RSo5Q4ilAUXh7kpjyIP5odyc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe-ui-default.min.js" integrity="sha256-PWHOlUzc96pMc8ThwRIXPn8yH4NOLu42RQ0b9SpnpFk=" crossorigin="anonymous"></script>
<title>{{current_directory}}</title>
</head>
<body>
<h1>{{current_directory}}</h1>
<p><a href="index.html">Return to index</a></p>
<button id="opensessame">Re-open gallery</button>
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
<!-- Background of PhotoSwipe.
     It's a separate element, as animating opacity is faster than rgba(). -->
<div class="pswp__bg"></div>
<!-- Slides wrapper with overflow:hidden. -->
<div class="pswp__scroll-wrap">
    <!-- Container that holds slides.
      PhotoSwipe keeps only 3 of them in DOM to save memory.
      Don't modify these 3 pswp__item elements, data is added later on. -->
    <div class="pswp__container">
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
    </div>
    <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
    <div class="pswp__ui pswp__ui--hidden">
    <div class="pswp__top-bar">
      <!--  Controls are self-explanatory. Order can be changed. -->
      <div class="pswp__counter"></div>
      <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
      <button class="pswp__button pswp__button--share" title="Share"></button>
      <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
      <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
      <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
      <!-- element will get class pswp__preloader--active when preloader is running -->
      <div class="pswp__preloader">
        <div class="pswp__preloader__icn">
          <div class="pswp__preloader__cut">
            <div class="pswp__preloader__donut"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
      <div class="pswp__share-tooltip"></div>
    </div>
    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
    </button>
    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
    </button>
    <div class="pswp__caption">
      <div class="pswp__caption__center"></div>
    </div>
    </div>
    </div>
</div>
<script>
var openPhotoSwipe = function() {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // build items array
    var items = {{itemjson}};

    // define options (if needed)
    var options = {
        // optionName: 'option value'
        index: 0,
    };

    // Initializes and opens PhotoSwipe
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};
openPhotoSwipe();
document.getElementById('opensessame').onclick = openPhotoSwipe;
</script>
</body>
</html>
""")


if __name__ == "__main__":
    main()

